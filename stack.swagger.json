{
  "swagger": "2.0",
  "info": {
    "title": "Stacks API",
    "version": "1.0",
    "contact": {
      "name": "StackPath Support",
      "url": "https://support.stackpath.com/"
    }
  },
  "host": "gateway.stackpath.com",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/stack/v1/stacks": {
      "get": {
        "summary": "GetStacks returns stacks",
        "operationId": "GetStacks",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/stackGetStacksResponse"
            }
          },
          "401": {
            "description": "Returned when an unauthorized request is attempted.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "500": {
            "description": "Internal server error.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "default": {
            "description": "Default error structure.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "page_request.first",
            "description": "first is the number of items desired.",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "page_request.after",
            "description": "after is the cursor value after which data will be returned.",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "page_request.filter",
            "description": "filter will accept sql style constraints.",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "page_request.sort_by",
            "description": "sort_by will sort the response by the given field.",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "account_id",
            "description": "For now account_id is required as a query parameter. This may change in the future.",
            "in": "query",
            "required": false,
            "type": "string"
          }
        ],
        "tags": [
          "Stack"
        ]
      },
      "post": {
        "summary": "CreateStack creates a stack on an account",
        "operationId": "CreateStack",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/stackCreateStackResponse"
            }
          },
          "401": {
            "description": "Returned when an unauthorized request is attempted.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "500": {
            "description": "Internal server error.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "default": {
            "description": "Default error structure.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/stackCreateStackRequest"
            }
          }
        ],
        "tags": [
          "Stack"
        ]
      }
    },
    "/stack/v1/stacks/{stack_id}": {
      "get": {
        "summary": "GetStack returns a stack",
        "operationId": "GetStack",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/stackStack"
            }
          },
          "401": {
            "description": "Returned when an unauthorized request is attempted.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "500": {
            "description": "Internal server error.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "default": {
            "description": "Default error structure.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "stack_id",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "Stack"
        ]
      },
      "post": {
        "summary": "UpdateStack updates a stack",
        "operationId": "UpdateStack",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/stackUpdateStackResponse"
            }
          },
          "401": {
            "description": "Returned when an unauthorized request is attempted.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "500": {
            "description": "Internal server error.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          },
          "default": {
            "description": "Default error structure.",
            "schema": {
              "$ref": "#/definitions/stackpathapiStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "stack_id",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/stackUpdateStackRequest"
            }
          }
        ],
        "tags": [
          "Stack"
        ]
      }
    }
  },
  "definitions": {
    "apiStatusDetail": {
      "type": "object",
      "properties": {
        "@type": {
          "type": "string"
        }
      },
      "required": [
        "@type"
      ],
      "discriminator": "@type"
    },
    "paginationPageInfo": {
      "type": "object",
      "properties": {
        "totalCount": {
          "type": "string",
          "title": "total_count is the total number of items in the dataset"
        },
        "hasPreviousPage": {
          "type": "boolean",
          "format": "boolean",
          "title": "has_previous_page will be true when a previous page of data exists"
        },
        "hasNextPage": {
          "type": "boolean",
          "format": "boolean",
          "title": "has_next_page will be true when another page of data is available"
        },
        "startCursor": {
          "type": "string",
          "title": "start_cursor will be the cursor for the first item in the set of data returned"
        },
        "endCursor": {
          "type": "string",
          "title": "end_cursor will be the cursor for the last item in the set of data returned"
        }
      },
      "description": "PageInfo provides information about a paginated response.\nThis is modeleted after the GraphQL Relay spec to support both cursor\nbased pagination and traditional offset based pagination."
    },
    "paginationPageRequest": {
      "type": "object",
      "properties": {
        "first": {
          "type": "string",
          "title": "first is the number of items desired"
        },
        "after": {
          "type": "string",
          "title": "after is the cursor value after which data will be returned"
        },
        "filter": {
          "type": "string",
          "title": "filter will accept sql style constraints"
        },
        "sortBy": {
          "type": "string",
          "title": "sort_by will sort the response by the given field"
        }
      },
      "description": "PageRequest is provided to a call which accepts paginated requests.\nThis is modeled after the GraphQL Relay spec to support both cursor\nbased pagination and traditional offset based pagination."
    },
    "stackCreateStackRequest": {
      "type": "object",
      "properties": {
        "accountId": {
          "type": "string"
        },
        "name": {
          "type": "string"
        }
      }
    },
    "stackCreateStackResponse": {
      "type": "object",
      "properties": {
        "stack": {
          "$ref": "#/definitions/stackStack"
        }
      }
    },
    "stackGetStacksResponse": {
      "type": "object",
      "properties": {
        "pageInfo": {
          "$ref": "#/definitions/paginationPageInfo"
        },
        "results": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/stackStack"
          }
        }
      },
      "title": "GetStacksRequest returns stacks"
    },
    "stackStack": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string"
        },
        "accountId": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time"
        },
        "status": {
          "$ref": "#/definitions/stackStackStatus"
        }
      }
    },
    "stackStackStatus": {
      "type": "string",
      "enum": [
        "PENDING",
        "ACTIVE",
        "DISABLED",
        "SUSPENDED",
        "BILLING_SUSPENDED",
        "CANCELLED",
        "DELETED"
      ],
      "default": "PENDING"
    },
    "stackUpdateStackRequest": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        }
      }
    },
    "stackUpdateStackResponse": {
      "type": "object",
      "properties": {
        "stack": {
          "$ref": "#/definitions/stackStack"
        }
      }
    },
    "stackpath.rpc.BadRequest": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "fieldViolations": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/stackpath.rpc.BadRequest.FieldViolation"
              }
            }
          }
        }
      ]
    },
    "stackpath.rpc.BadRequest.FieldViolation": {
      "type": "object",
      "properties": {
        "field": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "stackpath.rpc.Help": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "links": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/stackpath.rpc.Help.Link"
              }
            }
          }
        }
      ]
    },
    "stackpath.rpc.Help.Link": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "url": {
          "type": "string"
        }
      }
    },
    "stackpath.rpc.LocalizedMessage": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "locale": {
              "type": "string"
            },
            "message": {
              "type": "string"
            }
          }
        }
      ]
    },
    "stackpath.rpc.PreconditionFailure": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "violations": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/stackpath.rpc.PreconditionFailure.Violation"
              }
            }
          }
        }
      ]
    },
    "stackpath.rpc.PreconditionFailure.Violation": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "subject": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "stackpath.rpc.QuotaFailure": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "violations": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/stackpath.rpc.QuotaFailure.Violation"
              }
            }
          }
        }
      ]
    },
    "stackpath.rpc.QuotaFailure.Violation": {
      "type": "object",
      "properties": {
        "subject": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "stackpath.rpc.RequestInfo": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "requestId": {
              "type": "string"
            },
            "servingData": {
              "type": "string"
            }
          }
        }
      ]
    },
    "stackpath.rpc.ResourceInfo": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "resourceType": {
              "type": "string"
            },
            "resourceName": {
              "type": "string"
            },
            "owner": {
              "type": "string"
            },
            "description": {
              "type": "string"
            }
          }
        }
      ]
    },
    "stackpath.rpc.RetryInfo": {
      "allOf": [
        {
          "$ref": "#/definitions/apiStatusDetail"
        },
        {
          "type": "object",
          "properties": {
            "retryDelay": {
              "type": "string"
            }
          }
        }
      ]
    },
    "stackpathapiStatus": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "error": {
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/apiStatusDetail"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "application",
      "tokenUrl": "https://gateway.stackpath.com/identity/v1/oauth2/token"
    }
  },
  "security": [
    {
      "OAuth2": []
    }
  ]
}
