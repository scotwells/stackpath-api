Encryptme API Client
======

This library provides generated clients for the encryptme brand API

Using the client
================

The client is generated using [go-swagger](https://github.com/go-swagger/go-swagger). This means the generated client
makes use of [github.com/go-openapi/runtime/client](https://godoc.org/github.com/go-openapi/runtime/client). If you need
a more complex configuration, it is probably possible with a combination of this generated client and the runtime
options.

The [godocs for the go-openapi/runtime/client module](https://godoc.org/github.com/go-openapi/runtime/client) explain
the client options in detail, including different authentication and debugging options. One thing I want to flag because
it is so useful: setting the `DEBUG` environment variable will dump all requests to standard out.

Regenerating the client
=======================

To regenerate the client with a new or different swagger schema, first clean the existing client, then replace
[swagger.json](https://gitlab.stackpath.net/encryptme/backend/python/raw/master/cloak/web/brandapi-swagger.json), and finally re-generate:
```
make clean
curl -O swagger.json https://gitlab.stackpath.net/encryptme/backend/python/raw/master/cloak/web/brandapi-swagger.json
make generate
```
