// Code generated by go-swagger; DO NOT EDIT.

package identity

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/identity/models"
)

// GetUserCredentialsReader is a Reader for the GetUserCredentials structure.
type GetUserCredentialsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetUserCredentialsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetUserCredentialsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetUserCredentialsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewGetUserCredentialsInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewGetUserCredentialsDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewGetUserCredentialsOK creates a GetUserCredentialsOK with default headers values
func NewGetUserCredentialsOK() *GetUserCredentialsOK {
	return &GetUserCredentialsOK{}
}

/*GetUserCredentialsOK handles this case with default header values.

GetUserCredentialsOK get user credentials Ok
*/
type GetUserCredentialsOK struct {
	Payload *models.IdentityGetUserCredentialsResponse
}

func (o *GetUserCredentialsOK) Error() string {
	return fmt.Sprintf("[GET /identity/v1/users/{user_id}/credentials][%d] getUserCredentialsOk  %+v", 200, o.Payload)
}

func (o *GetUserCredentialsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.IdentityGetUserCredentialsResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetUserCredentialsUnauthorized creates a GetUserCredentialsUnauthorized with default headers values
func NewGetUserCredentialsUnauthorized() *GetUserCredentialsUnauthorized {
	return &GetUserCredentialsUnauthorized{}
}

/*GetUserCredentialsUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type GetUserCredentialsUnauthorized struct {
	Payload *models.StackpathapiStatus
}

func (o *GetUserCredentialsUnauthorized) Error() string {
	return fmt.Sprintf("[GET /identity/v1/users/{user_id}/credentials][%d] getUserCredentialsUnauthorized  %+v", 401, o.Payload)
}

func (o *GetUserCredentialsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetUserCredentialsInternalServerError creates a GetUserCredentialsInternalServerError with default headers values
func NewGetUserCredentialsInternalServerError() *GetUserCredentialsInternalServerError {
	return &GetUserCredentialsInternalServerError{}
}

/*GetUserCredentialsInternalServerError handles this case with default header values.

Internal server error.
*/
type GetUserCredentialsInternalServerError struct {
	Payload *models.StackpathapiStatus
}

func (o *GetUserCredentialsInternalServerError) Error() string {
	return fmt.Sprintf("[GET /identity/v1/users/{user_id}/credentials][%d] getUserCredentialsInternalServerError  %+v", 500, o.Payload)
}

func (o *GetUserCredentialsInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetUserCredentialsDefault creates a GetUserCredentialsDefault with default headers values
func NewGetUserCredentialsDefault(code int) *GetUserCredentialsDefault {
	return &GetUserCredentialsDefault{
		_statusCode: code,
	}
}

/*GetUserCredentialsDefault handles this case with default header values.

Default error structure.
*/
type GetUserCredentialsDefault struct {
	_statusCode int

	Payload *models.StackpathapiStatus
}

// Code gets the status code for the get user credentials default response
func (o *GetUserCredentialsDefault) Code() int {
	return o._statusCode
}

func (o *GetUserCredentialsDefault) Error() string {
	return fmt.Sprintf("[GET /identity/v1/users/{user_id}/credentials][%d] GetUserCredentials default  %+v", o._statusCode, o.Payload)
}

func (o *GetUserCredentialsDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
