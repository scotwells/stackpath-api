// Code generated by go-swagger; DO NOT EDIT.

package identity

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteUserCredentialParams creates a new DeleteUserCredentialParams object
// with the default values initialized.
func NewDeleteUserCredentialParams() *DeleteUserCredentialParams {
	var ()
	return &DeleteUserCredentialParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteUserCredentialParamsWithTimeout creates a new DeleteUserCredentialParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteUserCredentialParamsWithTimeout(timeout time.Duration) *DeleteUserCredentialParams {
	var ()
	return &DeleteUserCredentialParams{

		timeout: timeout,
	}
}

// NewDeleteUserCredentialParamsWithContext creates a new DeleteUserCredentialParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteUserCredentialParamsWithContext(ctx context.Context) *DeleteUserCredentialParams {
	var ()
	return &DeleteUserCredentialParams{

		Context: ctx,
	}
}

// NewDeleteUserCredentialParamsWithHTTPClient creates a new DeleteUserCredentialParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteUserCredentialParamsWithHTTPClient(client *http.Client) *DeleteUserCredentialParams {
	var ()
	return &DeleteUserCredentialParams{
		HTTPClient: client,
	}
}

/*DeleteUserCredentialParams contains all the parameters to send to the API endpoint
for the delete user credential operation typically these are written to a http.Request
*/
type DeleteUserCredentialParams struct {

	/*CredentialID*/
	CredentialID string
	/*UserID*/
	UserID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete user credential params
func (o *DeleteUserCredentialParams) WithTimeout(timeout time.Duration) *DeleteUserCredentialParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete user credential params
func (o *DeleteUserCredentialParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete user credential params
func (o *DeleteUserCredentialParams) WithContext(ctx context.Context) *DeleteUserCredentialParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete user credential params
func (o *DeleteUserCredentialParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete user credential params
func (o *DeleteUserCredentialParams) WithHTTPClient(client *http.Client) *DeleteUserCredentialParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete user credential params
func (o *DeleteUserCredentialParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithCredentialID adds the credentialID to the delete user credential params
func (o *DeleteUserCredentialParams) WithCredentialID(credentialID string) *DeleteUserCredentialParams {
	o.SetCredentialID(credentialID)
	return o
}

// SetCredentialID adds the credentialId to the delete user credential params
func (o *DeleteUserCredentialParams) SetCredentialID(credentialID string) {
	o.CredentialID = credentialID
}

// WithUserID adds the userID to the delete user credential params
func (o *DeleteUserCredentialParams) WithUserID(userID string) *DeleteUserCredentialParams {
	o.SetUserID(userID)
	return o
}

// SetUserID adds the userId to the delete user credential params
func (o *DeleteUserCredentialParams) SetUserID(userID string) {
	o.UserID = userID
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteUserCredentialParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param credential_id
	if err := r.SetPathParam("credential_id", o.CredentialID); err != nil {
		return err
	}

	// path param user_id
	if err := r.SetPathParam("user_id", o.UserID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
