// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// IdentityGetAccessTokenRequest identity get access token request
// swagger:model identityGetAccessTokenRequest
type IdentityGetAccessTokenRequest struct {

	// The client_id from your API credential.
	ClientID string `json:"client_id,omitempty"`

	// The client_secret from your API credential.
	ClientSecret string `json:"client_secret,omitempty"`

	// The OAuth2 Grant Type. Currently the only supported grant_type is: client_credentials
	GrantType string `json:"grant_type,omitempty"`

	// Scopes is currently not implemented. The token returned will have full access to the credential's capabilities.
	Scope string `json:"scope,omitempty"`
}

// Validate validates this identity get access token request
func (m *IdentityGetAccessTokenRequest) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *IdentityGetAccessTokenRequest) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *IdentityGetAccessTokenRequest) UnmarshalBinary(b []byte) error {
	var res IdentityGetAccessTokenRequest
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
