// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// SiteTypeValue site type value
// swagger:model SiteTypeValue
type SiteTypeValue string

const (

	// SiteTypeValueUNKNOWN captures enum value "UNKNOWN"
	SiteTypeValueUNKNOWN SiteTypeValue = "UNKNOWN"

	// SiteTypeValueCDN captures enum value "CDN"
	SiteTypeValueCDN SiteTypeValue = "CDN"

	// SiteTypeValueWAF captures enum value "WAF"
	SiteTypeValueWAF SiteTypeValue = "WAF"

	// SiteTypeValueAPI captures enum value "API"
	SiteTypeValueAPI SiteTypeValue = "API"
)

// for schema
var siteTypeValueEnum []interface{}

func init() {
	var res []SiteTypeValue
	if err := json.Unmarshal([]byte(`["UNKNOWN","CDN","WAF","API"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		siteTypeValueEnum = append(siteTypeValueEnum, v)
	}
}

func (m SiteTypeValue) validateSiteTypeValueEnum(path, location string, value SiteTypeValue) error {
	if err := validate.Enum(path, location, value, siteTypeValueEnum); err != nil {
		return err
	}
	return nil
}

// Validate validates this site type value
func (m SiteTypeValue) Validate(formats strfmt.Registry) error {
	var res []error

	// value enum
	if err := m.validateSiteTypeValueEnum("", "body", m); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
