// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ScanOriginResponseOriginScanSSLDetails scan origin response origin scan s s l details
// swagger:model ScanOriginResponseOriginScanSSLDetails
type ScanOriginResponseOriginScanSSLDetails struct {

	// error
	Error string `json:"error,omitempty"`

	// valid
	Valid bool `json:"valid,omitempty"`
}

// Validate validates this scan origin response origin scan s s l details
func (m *ScanOriginResponseOriginScanSSLDetails) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ScanOriginResponseOriginScanSSLDetails) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ScanOriginResponseOriginScanSSLDetails) UnmarshalBinary(b []byte) error {
	var res ScanOriginResponseOriginScanSSLDetails
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
