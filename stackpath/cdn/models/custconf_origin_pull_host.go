// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// CustconfOriginPullHost custconf origin pull host
// swagger:model custconfOriginPullHost
type CustconfOriginPullHost struct {

	// This is used by the API to perform conflict checking.
	ID string `json:"id,omitempty"`

	// origin Url
	OriginURL string `json:"originUrl,omitempty"`

	// password
	Password string `json:"password,omitempty"`

	// user name
	UserName string `json:"userName,omitempty"`
}

// Validate validates this custconf origin pull host
func (m *CustconfOriginPullHost) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *CustconfOriginPullHost) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *CustconfOriginPullHost) UnmarshalBinary(b []byte) error {
	var res CustconfOriginPullHost
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
