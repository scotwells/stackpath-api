// Code generated by go-swagger; DO NOT EDIT.

package event

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new event API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) ClientInterface {
	return &Client{transport: transport, formats: formats}
}

/*
Client for event API
*/

// ClientInterface defines the client interface
type ClientInterface interface {
	GetEvent(params *GetEventParams, authInfo runtime.ClientAuthInfoWriter) (*GetEventOK, error)
	GetEventStatistics(params *GetEventStatisticsParams, authInfo runtime.ClientAuthInfoWriter) (*GetEventStatisticsOK, error)
	SearchEvents(params *SearchEventsParams, authInfo runtime.ClientAuthInfoWriter) (*SearchEventsOK, error)
	SetTransport(transport runtime.ClientTransport)
}

type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
GetEvent get event API
*/
func (a *Client) GetEvent(params *GetEventParams, authInfo runtime.ClientAuthInfoWriter) (*GetEventOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetEventParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetEvent",
		Method:             "GET",
		PathPattern:        "/waf/v1/stacks/{stack_id}/sites/{site_id}/events/{event_id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetEventReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetEventOK), nil

}

/*
GetEventStatistics get event statistics API
*/
func (a *Client) GetEventStatistics(params *GetEventStatisticsParams, authInfo runtime.ClientAuthInfoWriter) (*GetEventStatisticsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetEventStatisticsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetEventStatistics",
		Method:             "GET",
		PathPattern:        "/waf/v1/stacks/{stack_id}/sites/{site_id}/events/stats",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetEventStatisticsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetEventStatisticsOK), nil

}

/*
SearchEvents search events API
*/
func (a *Client) SearchEvents(params *SearchEventsParams, authInfo runtime.ClientAuthInfoWriter) (*SearchEventsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewSearchEventsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "SearchEvents",
		Method:             "GET",
		PathPattern:        "/waf/v1/stacks/{stack_id}/sites/{site_id}/events",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &SearchEventsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*SearchEventsOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
