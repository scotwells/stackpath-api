// Code generated by go-swagger; DO NOT EDIT.

package event

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/waf/models"
)

// SearchEventsReader is a Reader for the SearchEvents structure.
type SearchEventsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *SearchEventsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewSearchEventsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewSearchEventsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewSearchEventsInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewSearchEventsDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewSearchEventsOK creates a SearchEventsOK with default headers values
func NewSearchEventsOK() *SearchEventsOK {
	return &SearchEventsOK{}
}

/*SearchEventsOK handles this case with default header values.

SearchEventsOK search events Ok
*/
type SearchEventsOK struct {
	Payload *models.EventSearchEventsResponse
}

func (o *SearchEventsOK) Error() string {
	return fmt.Sprintf("[GET /waf/v1/stacks/{stack_id}/sites/{site_id}/events][%d] searchEventsOk  %+v", 200, o.Payload)
}

func (o *SearchEventsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.EventSearchEventsResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewSearchEventsUnauthorized creates a SearchEventsUnauthorized with default headers values
func NewSearchEventsUnauthorized() *SearchEventsUnauthorized {
	return &SearchEventsUnauthorized{}
}

/*SearchEventsUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type SearchEventsUnauthorized struct {
	Payload *models.StackpathapiStatus
}

func (o *SearchEventsUnauthorized) Error() string {
	return fmt.Sprintf("[GET /waf/v1/stacks/{stack_id}/sites/{site_id}/events][%d] searchEventsUnauthorized  %+v", 401, o.Payload)
}

func (o *SearchEventsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewSearchEventsInternalServerError creates a SearchEventsInternalServerError with default headers values
func NewSearchEventsInternalServerError() *SearchEventsInternalServerError {
	return &SearchEventsInternalServerError{}
}

/*SearchEventsInternalServerError handles this case with default header values.

Internal server error.
*/
type SearchEventsInternalServerError struct {
	Payload *models.StackpathapiStatus
}

func (o *SearchEventsInternalServerError) Error() string {
	return fmt.Sprintf("[GET /waf/v1/stacks/{stack_id}/sites/{site_id}/events][%d] searchEventsInternalServerError  %+v", 500, o.Payload)
}

func (o *SearchEventsInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewSearchEventsDefault creates a SearchEventsDefault with default headers values
func NewSearchEventsDefault(code int) *SearchEventsDefault {
	return &SearchEventsDefault{
		_statusCode: code,
	}
}

/*SearchEventsDefault handles this case with default header values.

Default error structure.
*/
type SearchEventsDefault struct {
	_statusCode int

	Payload *models.StackpathapiStatus
}

// Code gets the status code for the search events default response
func (o *SearchEventsDefault) Code() int {
	return o._statusCode
}

func (o *SearchEventsDefault) Error() string {
	return fmt.Sprintf("[GET /waf/v1/stacks/{stack_id}/sites/{site_id}/events][%d] SearchEvents default  %+v", o._statusCode, o.Payload)
}

func (o *SearchEventsDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
