// Code generated by go-swagger; DO NOT EDIT.

package rule

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/waf/models"
)

// NewEnableRuleParams creates a new EnableRuleParams object
// with the default values initialized.
func NewEnableRuleParams() *EnableRuleParams {
	var ()
	return &EnableRuleParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewEnableRuleParamsWithTimeout creates a new EnableRuleParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewEnableRuleParamsWithTimeout(timeout time.Duration) *EnableRuleParams {
	var ()
	return &EnableRuleParams{

		timeout: timeout,
	}
}

// NewEnableRuleParamsWithContext creates a new EnableRuleParams object
// with the default values initialized, and the ability to set a context for a request
func NewEnableRuleParamsWithContext(ctx context.Context) *EnableRuleParams {
	var ()
	return &EnableRuleParams{

		Context: ctx,
	}
}

// NewEnableRuleParamsWithHTTPClient creates a new EnableRuleParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewEnableRuleParamsWithHTTPClient(client *http.Client) *EnableRuleParams {
	var ()
	return &EnableRuleParams{
		HTTPClient: client,
	}
}

/*EnableRuleParams contains all the parameters to send to the API endpoint
for the enable rule operation typically these are written to a http.Request
*/
type EnableRuleParams struct {

	/*Body*/
	Body models.RuleEnableRuleRequest
	/*RuleID*/
	RuleID string
	/*SiteID*/
	SiteID string
	/*StackID*/
	StackID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the enable rule params
func (o *EnableRuleParams) WithTimeout(timeout time.Duration) *EnableRuleParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the enable rule params
func (o *EnableRuleParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the enable rule params
func (o *EnableRuleParams) WithContext(ctx context.Context) *EnableRuleParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the enable rule params
func (o *EnableRuleParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the enable rule params
func (o *EnableRuleParams) WithHTTPClient(client *http.Client) *EnableRuleParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the enable rule params
func (o *EnableRuleParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the enable rule params
func (o *EnableRuleParams) WithBody(body models.RuleEnableRuleRequest) *EnableRuleParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the enable rule params
func (o *EnableRuleParams) SetBody(body models.RuleEnableRuleRequest) {
	o.Body = body
}

// WithRuleID adds the ruleID to the enable rule params
func (o *EnableRuleParams) WithRuleID(ruleID string) *EnableRuleParams {
	o.SetRuleID(ruleID)
	return o
}

// SetRuleID adds the ruleId to the enable rule params
func (o *EnableRuleParams) SetRuleID(ruleID string) {
	o.RuleID = ruleID
}

// WithSiteID adds the siteID to the enable rule params
func (o *EnableRuleParams) WithSiteID(siteID string) *EnableRuleParams {
	o.SetSiteID(siteID)
	return o
}

// SetSiteID adds the siteId to the enable rule params
func (o *EnableRuleParams) SetSiteID(siteID string) {
	o.SiteID = siteID
}

// WithStackID adds the stackID to the enable rule params
func (o *EnableRuleParams) WithStackID(stackID string) *EnableRuleParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the enable rule params
func (o *EnableRuleParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WriteToRequest writes these params to a swagger request
func (o *EnableRuleParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param rule_id
	if err := r.SetPathParam("rule_id", o.RuleID); err != nil {
		return err
	}

	// path param site_id
	if err := r.SetPathParam("site_id", o.SiteID); err != nil {
		return err
	}

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
