// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ConditionHeaderCondition Match an incoming request header
// swagger:model ConditionHeaderCondition
type ConditionHeaderCondition struct {

	// exact match
	ExactMatch bool `json:"exactMatch,omitempty"`

	// header
	Header string `json:"header,omitempty"`

	// value
	Value string `json:"value,omitempty"`
}

// Validate validates this condition header condition
func (m *ConditionHeaderCondition) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ConditionHeaderCondition) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ConditionHeaderCondition) UnmarshalBinary(b []byte) error {
	var res ConditionHeaderCondition
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
