// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ConditionCountryCondition Match the country that the requested originated from
// swagger:model ConditionCountryCondition
type ConditionCountryCondition struct {

	// An ISO 3166-1 alpha-2 formatted string
	CountryCode string `json:"countryCode,omitempty"`
}

// Validate validates this condition country condition
func (m *ConditionCountryCondition) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ConditionCountryCondition) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ConditionCountryCondition) UnmarshalBinary(b []byte) error {
	var res ConditionCountryCondition
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
