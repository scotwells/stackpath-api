// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// SearchEventsRequestSort search events request sort
// swagger:model SearchEventsRequestSort
type SearchEventsRequestSort string

const (

	// SearchEventsRequestSortTIMESTAMP captures enum value "TIMESTAMP"
	SearchEventsRequestSortTIMESTAMP SearchEventsRequestSort = "TIMESTAMP"

	// SearchEventsRequestSortCOUNTRY captures enum value "COUNTRY"
	SearchEventsRequestSortCOUNTRY SearchEventsRequestSort = "COUNTRY"

	// SearchEventsRequestSortRULENAME captures enum value "RULE_NAME"
	SearchEventsRequestSortRULENAME SearchEventsRequestSort = "RULE_NAME"
)

// for schema
var searchEventsRequestSortEnum []interface{}

func init() {
	var res []SearchEventsRequestSort
	if err := json.Unmarshal([]byte(`["TIMESTAMP","COUNTRY","RULE_NAME"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		searchEventsRequestSortEnum = append(searchEventsRequestSortEnum, v)
	}
}

func (m SearchEventsRequestSort) validateSearchEventsRequestSortEnum(path, location string, value SearchEventsRequestSort) error {
	if err := validate.Enum(path, location, value, searchEventsRequestSortEnum); err != nil {
		return err
	}
	return nil
}

// Validate validates this search events request sort
func (m SearchEventsRequestSort) Validate(formats strfmt.Registry) error {
	var res []error

	// value enum
	if err := m.validateSearchEventsRequestSortEnum("", "body", m); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
