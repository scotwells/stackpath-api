// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// SiteUpdateSiteAPIUrlsResponse site update site Api urls response
// swagger:model siteUpdateSiteApiUrlsResponse
type SiteUpdateSiteAPIUrlsResponse struct {

	// api urls
	APIUrls []string `json:"apiUrls"`
}

// Validate validates this site update site Api urls response
func (m *SiteUpdateSiteAPIUrlsResponse) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *SiteUpdateSiteAPIUrlsResponse) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SiteUpdateSiteAPIUrlsResponse) UnmarshalBinary(b []byte) error {
	var res SiteUpdateSiteAPIUrlsResponse
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
