// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// PolicyPolicyAction The potential actions that the WAF will take when a policy is triggered
// swagger:model policyPolicyAction
type PolicyPolicyAction string

const (

	// PolicyPolicyActionBLOCK captures enum value "BLOCK"
	PolicyPolicyActionBLOCK PolicyPolicyAction = "BLOCK"

	// PolicyPolicyActionALLOW captures enum value "ALLOW"
	PolicyPolicyActionALLOW PolicyPolicyAction = "ALLOW"

	// PolicyPolicyActionCAPTCHA captures enum value "CAPTCHA"
	PolicyPolicyActionCAPTCHA PolicyPolicyAction = "CAPTCHA"

	// PolicyPolicyActionHANDSHAKE captures enum value "HANDSHAKE"
	PolicyPolicyActionHANDSHAKE PolicyPolicyAction = "HANDSHAKE"

	// PolicyPolicyActionMONITOR captures enum value "MONITOR"
	PolicyPolicyActionMONITOR PolicyPolicyAction = "MONITOR"
)

// for schema
var policyPolicyActionEnum []interface{}

func init() {
	var res []PolicyPolicyAction
	if err := json.Unmarshal([]byte(`["BLOCK","ALLOW","CAPTCHA","HANDSHAKE","MONITOR"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		policyPolicyActionEnum = append(policyPolicyActionEnum, v)
	}
}

func (m PolicyPolicyAction) validatePolicyPolicyActionEnum(path, location string, value PolicyPolicyAction) error {
	if err := validate.Enum(path, location, value, policyPolicyActionEnum); err != nil {
		return err
	}
	return nil
}

// Validate validates this policy policy action
func (m PolicyPolicyAction) Validate(formats strfmt.Registry) error {
	var res []error

	// value enum
	if err := m.validatePolicyPolicyActionEnum("", "body", m); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
