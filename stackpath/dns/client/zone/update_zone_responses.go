// Code generated by go-swagger; DO NOT EDIT.

package zone

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/dns/models"
)

// UpdateZoneReader is a Reader for the UpdateZone structure.
type UpdateZoneReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UpdateZoneReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewUpdateZoneOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewUpdateZoneUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewUpdateZoneInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewUpdateZoneDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewUpdateZoneOK creates a UpdateZoneOK with default headers values
func NewUpdateZoneOK() *UpdateZoneOK {
	return &UpdateZoneOK{}
}

/*UpdateZoneOK handles this case with default header values.

UpdateZoneOK update zone Ok
*/
type UpdateZoneOK struct {
	Payload *models.ZoneUpdateZoneResponse
}

func (o *UpdateZoneOK) Error() string {
	return fmt.Sprintf("[PUT /dns/v1/stacks/{stack_id}/zones/{zone_id}][%d] updateZoneOk  %+v", 200, o.Payload)
}

func (o *UpdateZoneOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ZoneUpdateZoneResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewUpdateZoneUnauthorized creates a UpdateZoneUnauthorized with default headers values
func NewUpdateZoneUnauthorized() *UpdateZoneUnauthorized {
	return &UpdateZoneUnauthorized{}
}

/*UpdateZoneUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type UpdateZoneUnauthorized struct {
	Payload *models.StackpathapiStatus
}

func (o *UpdateZoneUnauthorized) Error() string {
	return fmt.Sprintf("[PUT /dns/v1/stacks/{stack_id}/zones/{zone_id}][%d] updateZoneUnauthorized  %+v", 401, o.Payload)
}

func (o *UpdateZoneUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewUpdateZoneInternalServerError creates a UpdateZoneInternalServerError with default headers values
func NewUpdateZoneInternalServerError() *UpdateZoneInternalServerError {
	return &UpdateZoneInternalServerError{}
}

/*UpdateZoneInternalServerError handles this case with default header values.

Internal server error.
*/
type UpdateZoneInternalServerError struct {
	Payload *models.StackpathapiStatus
}

func (o *UpdateZoneInternalServerError) Error() string {
	return fmt.Sprintf("[PUT /dns/v1/stacks/{stack_id}/zones/{zone_id}][%d] updateZoneInternalServerError  %+v", 500, o.Payload)
}

func (o *UpdateZoneInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewUpdateZoneDefault creates a UpdateZoneDefault with default headers values
func NewUpdateZoneDefault(code int) *UpdateZoneDefault {
	return &UpdateZoneDefault{
		_statusCode: code,
	}
}

/*UpdateZoneDefault handles this case with default header values.

Default error structure.
*/
type UpdateZoneDefault struct {
	_statusCode int

	Payload *models.StackpathapiStatus
}

// Code gets the status code for the update zone default response
func (o *UpdateZoneDefault) Code() int {
	return o._statusCode
}

func (o *UpdateZoneDefault) Error() string {
	return fmt.Sprintf("[PUT /dns/v1/stacks/{stack_id}/zones/{zone_id}][%d] UpdateZone default  %+v", o._statusCode, o.Payload)
}

func (o *UpdateZoneDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
