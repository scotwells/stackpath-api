// Code generated by go-swagger; DO NOT EDIT.

package zone

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/dns/models"
)

// NewBulkCreateOrUpdateZoneRecordsParams creates a new BulkCreateOrUpdateZoneRecordsParams object
// with the default values initialized.
func NewBulkCreateOrUpdateZoneRecordsParams() *BulkCreateOrUpdateZoneRecordsParams {
	var ()
	return &BulkCreateOrUpdateZoneRecordsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewBulkCreateOrUpdateZoneRecordsParamsWithTimeout creates a new BulkCreateOrUpdateZoneRecordsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewBulkCreateOrUpdateZoneRecordsParamsWithTimeout(timeout time.Duration) *BulkCreateOrUpdateZoneRecordsParams {
	var ()
	return &BulkCreateOrUpdateZoneRecordsParams{

		timeout: timeout,
	}
}

// NewBulkCreateOrUpdateZoneRecordsParamsWithContext creates a new BulkCreateOrUpdateZoneRecordsParams object
// with the default values initialized, and the ability to set a context for a request
func NewBulkCreateOrUpdateZoneRecordsParamsWithContext(ctx context.Context) *BulkCreateOrUpdateZoneRecordsParams {
	var ()
	return &BulkCreateOrUpdateZoneRecordsParams{

		Context: ctx,
	}
}

// NewBulkCreateOrUpdateZoneRecordsParamsWithHTTPClient creates a new BulkCreateOrUpdateZoneRecordsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewBulkCreateOrUpdateZoneRecordsParamsWithHTTPClient(client *http.Client) *BulkCreateOrUpdateZoneRecordsParams {
	var ()
	return &BulkCreateOrUpdateZoneRecordsParams{
		HTTPClient: client,
	}
}

/*BulkCreateOrUpdateZoneRecordsParams contains all the parameters to send to the API endpoint
for the bulk create or update zone records operation typically these are written to a http.Request
*/
type BulkCreateOrUpdateZoneRecordsParams struct {

	/*Body*/
	Body *models.ZoneBulkCreateOrUpdateZoneRecordsRequest
	/*StackID*/
	StackID string
	/*ZoneID*/
	ZoneID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithTimeout(timeout time.Duration) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithContext(ctx context.Context) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithHTTPClient(client *http.Client) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithBody(body *models.ZoneBulkCreateOrUpdateZoneRecordsRequest) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetBody(body *models.ZoneBulkCreateOrUpdateZoneRecordsRequest) {
	o.Body = body
}

// WithStackID adds the stackID to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithStackID(stackID string) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WithZoneID adds the zoneID to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) WithZoneID(zoneID string) *BulkCreateOrUpdateZoneRecordsParams {
	o.SetZoneID(zoneID)
	return o
}

// SetZoneID adds the zoneId to the bulk create or update zone records params
func (o *BulkCreateOrUpdateZoneRecordsParams) SetZoneID(zoneID string) {
	o.ZoneID = zoneID
}

// WriteToRequest writes these params to a swagger request
func (o *BulkCreateOrUpdateZoneRecordsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	// path param zone_id
	if err := r.SetPathParam("zone_id", o.ZoneID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
