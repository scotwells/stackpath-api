// Code generated by go-swagger; DO NOT EDIT.

package zone

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/dns/models"
)

// GetZoneRecordReader is a Reader for the GetZoneRecord structure.
type GetZoneRecordReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetZoneRecordReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetZoneRecordOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetZoneRecordUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewGetZoneRecordInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewGetZoneRecordDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewGetZoneRecordOK creates a GetZoneRecordOK with default headers values
func NewGetZoneRecordOK() *GetZoneRecordOK {
	return &GetZoneRecordOK{}
}

/*GetZoneRecordOK handles this case with default header values.

GetZoneRecordOK get zone record Ok
*/
type GetZoneRecordOK struct {
	Payload *models.ZoneGetZoneRecordResponse
}

func (o *GetZoneRecordOK) Error() string {
	return fmt.Sprintf("[GET /dns/v1/stacks/{stack_id}/zones/{zone_id}/records/{zone_record_id}][%d] getZoneRecordOk  %+v", 200, o.Payload)
}

func (o *GetZoneRecordOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ZoneGetZoneRecordResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetZoneRecordUnauthorized creates a GetZoneRecordUnauthorized with default headers values
func NewGetZoneRecordUnauthorized() *GetZoneRecordUnauthorized {
	return &GetZoneRecordUnauthorized{}
}

/*GetZoneRecordUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type GetZoneRecordUnauthorized struct {
	Payload *models.StackpathapiStatus
}

func (o *GetZoneRecordUnauthorized) Error() string {
	return fmt.Sprintf("[GET /dns/v1/stacks/{stack_id}/zones/{zone_id}/records/{zone_record_id}][%d] getZoneRecordUnauthorized  %+v", 401, o.Payload)
}

func (o *GetZoneRecordUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetZoneRecordInternalServerError creates a GetZoneRecordInternalServerError with default headers values
func NewGetZoneRecordInternalServerError() *GetZoneRecordInternalServerError {
	return &GetZoneRecordInternalServerError{}
}

/*GetZoneRecordInternalServerError handles this case with default header values.

Internal server error.
*/
type GetZoneRecordInternalServerError struct {
	Payload *models.StackpathapiStatus
}

func (o *GetZoneRecordInternalServerError) Error() string {
	return fmt.Sprintf("[GET /dns/v1/stacks/{stack_id}/zones/{zone_id}/records/{zone_record_id}][%d] getZoneRecordInternalServerError  %+v", 500, o.Payload)
}

func (o *GetZoneRecordInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetZoneRecordDefault creates a GetZoneRecordDefault with default headers values
func NewGetZoneRecordDefault(code int) *GetZoneRecordDefault {
	return &GetZoneRecordDefault{
		_statusCode: code,
	}
}

/*GetZoneRecordDefault handles this case with default header values.

Default error structure.
*/
type GetZoneRecordDefault struct {
	_statusCode int

	Payload *models.StackpathapiStatus
}

// Code gets the status code for the get zone record default response
func (o *GetZoneRecordDefault) Code() int {
	return o._statusCode
}

func (o *GetZoneRecordDefault) Error() string {
	return fmt.Sprintf("[GET /dns/v1/stacks/{stack_id}/zones/{zone_id}/records/{zone_record_id}][%d] GetZoneRecord default  %+v", o._statusCode, o.Payload)
}

func (o *GetZoneRecordDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.StackpathapiStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
