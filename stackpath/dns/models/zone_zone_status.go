// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// ZoneZoneStatus zone zone status
// swagger:model zoneZoneStatus
type ZoneZoneStatus string

const (

	// ZoneZoneStatusACTIVE captures enum value "ACTIVE"
	ZoneZoneStatusACTIVE ZoneZoneStatus = "ACTIVE"

	// ZoneZoneStatusSUSPENDED captures enum value "SUSPENDED"
	ZoneZoneStatusSUSPENDED ZoneZoneStatus = "SUSPENDED"

	// ZoneZoneStatusBILLINGSUSPENDED captures enum value "BILLING_SUSPENDED"
	ZoneZoneStatusBILLINGSUSPENDED ZoneZoneStatus = "BILLING_SUSPENDED"

	// ZoneZoneStatusINACTIVE captures enum value "INACTIVE"
	ZoneZoneStatusINACTIVE ZoneZoneStatus = "INACTIVE"
)

// for schema
var zoneZoneStatusEnum []interface{}

func init() {
	var res []ZoneZoneStatus
	if err := json.Unmarshal([]byte(`["ACTIVE","SUSPENDED","BILLING_SUSPENDED","INACTIVE"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		zoneZoneStatusEnum = append(zoneZoneStatusEnum, v)
	}
}

func (m ZoneZoneStatus) validateZoneZoneStatusEnum(path, location string, value ZoneZoneStatus) error {
	if err := validate.Enum(path, location, value, zoneZoneStatusEnum); err != nil {
		return err
	}
	return nil
}

// Validate validates this zone zone status
func (m ZoneZoneStatus) Validate(formats strfmt.Registry) error {
	var res []error

	// value enum
	if err := m.validateZoneZoneStatusEnum("", "body", m); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
