// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// ZoneDNSProvider zone Dns provider
// swagger:model zoneDnsProvider
type ZoneDNSProvider string

const (

	// ZoneDNSProviderGENERAL captures enum value "GENERAL"
	ZoneDNSProviderGENERAL ZoneDNSProvider = "GENERAL"

	// ZoneDNSProviderCLOUDFLARE captures enum value "CLOUDFLARE"
	ZoneDNSProviderCLOUDFLARE ZoneDNSProvider = "CLOUDFLARE"
)

// for schema
var zoneDnsProviderEnum []interface{}

func init() {
	var res []ZoneDNSProvider
	if err := json.Unmarshal([]byte(`["GENERAL","CLOUDFLARE"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		zoneDnsProviderEnum = append(zoneDnsProviderEnum, v)
	}
}

func (m ZoneDNSProvider) validateZoneDNSProviderEnum(path, location string, value ZoneDNSProvider) error {
	if err := validate.Enum(path, location, value, zoneDnsProviderEnum); err != nil {
		return err
	}
	return nil
}

// Validate validates this zone Dns provider
func (m ZoneDNSProvider) Validate(formats strfmt.Registry) error {
	var res []error

	// value enum
	if err := m.validateZoneDNSProviderEnum("", "body", m); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
