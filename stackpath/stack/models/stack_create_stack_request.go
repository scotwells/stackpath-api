// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// StackCreateStackRequest stack create stack request
// swagger:model stackCreateStackRequest
type StackCreateStackRequest struct {

	// account Id
	AccountID string `json:"accountId,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this stack create stack request
func (m *StackCreateStackRequest) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *StackCreateStackRequest) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StackCreateStackRequest) UnmarshalBinary(b []byte) error {
	var res StackCreateStackRequest
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
