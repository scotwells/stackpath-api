// Code generated by go-swagger; DO NOT EDIT.

package instance

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new instance API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) ClientInterface {
	return &Client{transport: transport, formats: formats}
}

/*
Client for instance API
*/

// ClientInterface defines the client interface
type ClientInterface interface {
	GetWorkloadInstances(params *GetWorkloadInstancesParams, authInfo runtime.ClientAuthInfoWriter) (*GetWorkloadInstancesOK, error)
	SetTransport(transport runtime.ClientTransport)
}

type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
GetWorkloadInstances retrieves a workload s instances
*/
func (a *Client) GetWorkloadInstances(params *GetWorkloadInstancesParams, authInfo runtime.ClientAuthInfoWriter) (*GetWorkloadInstancesOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetWorkloadInstancesParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetWorkloadInstances",
		Method:             "GET",
		PathPattern:        "/workload/v1/stacks/{stack_id}/workloads/{workload_id}/instances",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetWorkloadInstancesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetWorkloadInstancesOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
