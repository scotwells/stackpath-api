// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// MonitoringBulkDeleteChecksRequest monitoring bulk delete checks request
// swagger:model monitoringBulkDeleteChecksRequest
type MonitoringBulkDeleteChecksRequest struct {

	// check ids
	CheckIds []string `json:"checkIds"`
}

// Validate validates this monitoring bulk delete checks request
func (m *MonitoringBulkDeleteChecksRequest) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *MonitoringBulkDeleteChecksRequest) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *MonitoringBulkDeleteChecksRequest) UnmarshalBinary(b []byte) error {
	var res MonitoringBulkDeleteChecksRequest
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
