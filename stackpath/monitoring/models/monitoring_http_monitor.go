// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// MonitoringHTTPMonitor monitoring Http monitor
// swagger:model monitoringHttpMonitor
type MonitoringHTTPMonitor struct {

	// avg response time
	AvgResponseTime float64 `json:"avgResponseTime,omitempty"`

	// basic auth
	BasicAuth *HTTPMonitorBasicAuth `json:"basicAuth,omitempty"`

	// body
	Body string `json:"body,omitempty"`

	// current status
	CurrentStatus string `json:"currentStatus,omitempty"`

	// headers
	Headers map[string]string `json:"headers,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// ip version
	IPVersion HTTPMonitorIPVersion `json:"ipVersion,omitempty"`

	// locations
	Locations []*SchemamonitoringLocation `json:"locations"`

	// method
	Method MonitoringHTTPMonitorMethod `json:"method,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// remote Id
	RemoteID string `json:"remoteId,omitempty"`

	// timeout
	Timeout int32 `json:"timeout,omitempty"`

	// uptime
	Uptime float64 `json:"uptime,omitempty"`

	// url
	URL string `json:"url,omitempty"`

	// validate certificate
	ValidateCertificate bool `json:"validateCertificate,omitempty"`
}

// Validate validates this monitoring Http monitor
func (m *MonitoringHTTPMonitor) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateBasicAuth(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateIPVersion(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLocations(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateMethod(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *MonitoringHTTPMonitor) validateBasicAuth(formats strfmt.Registry) error {

	if swag.IsZero(m.BasicAuth) { // not required
		return nil
	}

	if m.BasicAuth != nil {
		if err := m.BasicAuth.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("basicAuth")
			}
			return err
		}
	}

	return nil
}

func (m *MonitoringHTTPMonitor) validateIPVersion(formats strfmt.Registry) error {

	if swag.IsZero(m.IPVersion) { // not required
		return nil
	}

	if err := m.IPVersion.Validate(formats); err != nil {
		if ve, ok := err.(*errors.Validation); ok {
			return ve.ValidateName("ipVersion")
		}
		return err
	}

	return nil
}

func (m *MonitoringHTTPMonitor) validateLocations(formats strfmt.Registry) error {

	if swag.IsZero(m.Locations) { // not required
		return nil
	}

	for i := 0; i < len(m.Locations); i++ {
		if swag.IsZero(m.Locations[i]) { // not required
			continue
		}

		if m.Locations[i] != nil {
			if err := m.Locations[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("locations" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *MonitoringHTTPMonitor) validateMethod(formats strfmt.Registry) error {

	if swag.IsZero(m.Method) { // not required
		return nil
	}

	if err := m.Method.Validate(formats); err != nil {
		if ve, ok := err.(*errors.Validation); ok {
			return ve.ValidateName("method")
		}
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *MonitoringHTTPMonitor) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *MonitoringHTTPMonitor) UnmarshalBinary(b []byte) error {
	var res MonitoringHTTPMonitor
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
