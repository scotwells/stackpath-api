// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// NewCreateCheckParams creates a new CreateCheckParams object
// with the default values initialized.
func NewCreateCheckParams() *CreateCheckParams {
	var ()
	return &CreateCheckParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewCreateCheckParamsWithTimeout creates a new CreateCheckParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewCreateCheckParamsWithTimeout(timeout time.Duration) *CreateCheckParams {
	var ()
	return &CreateCheckParams{

		timeout: timeout,
	}
}

// NewCreateCheckParamsWithContext creates a new CreateCheckParams object
// with the default values initialized, and the ability to set a context for a request
func NewCreateCheckParamsWithContext(ctx context.Context) *CreateCheckParams {
	var ()
	return &CreateCheckParams{

		Context: ctx,
	}
}

// NewCreateCheckParamsWithHTTPClient creates a new CreateCheckParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewCreateCheckParamsWithHTTPClient(client *http.Client) *CreateCheckParams {
	var ()
	return &CreateCheckParams{
		HTTPClient: client,
	}
}

/*CreateCheckParams contains all the parameters to send to the API endpoint
for the create check operation typically these are written to a http.Request
*/
type CreateCheckParams struct {

	/*Body*/
	Body *models.MonitoringCreateCheckRequest
	/*MonitorID*/
	MonitorID string
	/*StackID*/
	StackID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the create check params
func (o *CreateCheckParams) WithTimeout(timeout time.Duration) *CreateCheckParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the create check params
func (o *CreateCheckParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the create check params
func (o *CreateCheckParams) WithContext(ctx context.Context) *CreateCheckParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the create check params
func (o *CreateCheckParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the create check params
func (o *CreateCheckParams) WithHTTPClient(client *http.Client) *CreateCheckParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the create check params
func (o *CreateCheckParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the create check params
func (o *CreateCheckParams) WithBody(body *models.MonitoringCreateCheckRequest) *CreateCheckParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the create check params
func (o *CreateCheckParams) SetBody(body *models.MonitoringCreateCheckRequest) {
	o.Body = body
}

// WithMonitorID adds the monitorID to the create check params
func (o *CreateCheckParams) WithMonitorID(monitorID string) *CreateCheckParams {
	o.SetMonitorID(monitorID)
	return o
}

// SetMonitorID adds the monitorId to the create check params
func (o *CreateCheckParams) SetMonitorID(monitorID string) {
	o.MonitorID = monitorID
}

// WithStackID adds the stackID to the create check params
func (o *CreateCheckParams) WithStackID(stackID string) *CreateCheckParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the create check params
func (o *CreateCheckParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WriteToRequest writes these params to a swagger request
func (o *CreateCheckParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param monitor_id
	if err := r.SetPathParam("monitor_id", o.MonitorID); err != nil {
		return err
	}

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
