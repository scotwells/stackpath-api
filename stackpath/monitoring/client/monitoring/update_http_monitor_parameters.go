// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// NewUpdateHTTPMonitorParams creates a new UpdateHTTPMonitorParams object
// with the default values initialized.
func NewUpdateHTTPMonitorParams() *UpdateHTTPMonitorParams {
	var ()
	return &UpdateHTTPMonitorParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewUpdateHTTPMonitorParamsWithTimeout creates a new UpdateHTTPMonitorParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewUpdateHTTPMonitorParamsWithTimeout(timeout time.Duration) *UpdateHTTPMonitorParams {
	var ()
	return &UpdateHTTPMonitorParams{

		timeout: timeout,
	}
}

// NewUpdateHTTPMonitorParamsWithContext creates a new UpdateHTTPMonitorParams object
// with the default values initialized, and the ability to set a context for a request
func NewUpdateHTTPMonitorParamsWithContext(ctx context.Context) *UpdateHTTPMonitorParams {
	var ()
	return &UpdateHTTPMonitorParams{

		Context: ctx,
	}
}

// NewUpdateHTTPMonitorParamsWithHTTPClient creates a new UpdateHTTPMonitorParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewUpdateHTTPMonitorParamsWithHTTPClient(client *http.Client) *UpdateHTTPMonitorParams {
	var ()
	return &UpdateHTTPMonitorParams{
		HTTPClient: client,
	}
}

/*UpdateHTTPMonitorParams contains all the parameters to send to the API endpoint
for the update Http monitor operation typically these are written to a http.Request
*/
type UpdateHTTPMonitorParams struct {

	/*Body*/
	Body *models.MonitoringPatchHTTPMonitorMessage
	/*MonitorID*/
	MonitorID string
	/*StackID*/
	StackID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithTimeout(timeout time.Duration) *UpdateHTTPMonitorParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithContext(ctx context.Context) *UpdateHTTPMonitorParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithHTTPClient(client *http.Client) *UpdateHTTPMonitorParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithBody(body *models.MonitoringPatchHTTPMonitorMessage) *UpdateHTTPMonitorParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetBody(body *models.MonitoringPatchHTTPMonitorMessage) {
	o.Body = body
}

// WithMonitorID adds the monitorID to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithMonitorID(monitorID string) *UpdateHTTPMonitorParams {
	o.SetMonitorID(monitorID)
	return o
}

// SetMonitorID adds the monitorId to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetMonitorID(monitorID string) {
	o.MonitorID = monitorID
}

// WithStackID adds the stackID to the update Http monitor params
func (o *UpdateHTTPMonitorParams) WithStackID(stackID string) *UpdateHTTPMonitorParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the update Http monitor params
func (o *UpdateHTTPMonitorParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WriteToRequest writes these params to a swagger request
func (o *UpdateHTTPMonitorParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param monitor_id
	if err := r.SetPathParam("monitor_id", o.MonitorID); err != nil {
		return err
	}

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
