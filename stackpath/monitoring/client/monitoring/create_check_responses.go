// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// CreateCheckReader is a Reader for the CreateCheck structure.
type CreateCheckReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CreateCheckReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewCreateCheckOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewCreateCheckUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewCreateCheckInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewCreateCheckDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewCreateCheckOK creates a CreateCheckOK with default headers values
func NewCreateCheckOK() *CreateCheckOK {
	return &CreateCheckOK{}
}

/*CreateCheckOK handles this case with default header values.

CreateCheckOK create check Ok
*/
type CreateCheckOK struct {
	Payload *models.MonitoringCheck
}

func (o *CreateCheckOK) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/checks][%d] createCheckOk  %+v", 200, o.Payload)
}

func (o *CreateCheckOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.MonitoringCheck)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewCreateCheckUnauthorized creates a CreateCheckUnauthorized with default headers values
func NewCreateCheckUnauthorized() *CreateCheckUnauthorized {
	return &CreateCheckUnauthorized{}
}

/*CreateCheckUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type CreateCheckUnauthorized struct {
	Payload *models.APIStatus
}

func (o *CreateCheckUnauthorized) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/checks][%d] createCheckUnauthorized  %+v", 401, o.Payload)
}

func (o *CreateCheckUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewCreateCheckInternalServerError creates a CreateCheckInternalServerError with default headers values
func NewCreateCheckInternalServerError() *CreateCheckInternalServerError {
	return &CreateCheckInternalServerError{}
}

/*CreateCheckInternalServerError handles this case with default header values.

Internal server error.
*/
type CreateCheckInternalServerError struct {
	Payload *models.APIStatus
}

func (o *CreateCheckInternalServerError) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/checks][%d] createCheckInternalServerError  %+v", 500, o.Payload)
}

func (o *CreateCheckInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewCreateCheckDefault creates a CreateCheckDefault with default headers values
func NewCreateCheckDefault(code int) *CreateCheckDefault {
	return &CreateCheckDefault{
		_statusCode: code,
	}
}

/*CreateCheckDefault handles this case with default header values.

Default error structure.
*/
type CreateCheckDefault struct {
	_statusCode int

	Payload *models.APIStatus
}

// Code gets the status code for the create check default response
func (o *CreateCheckDefault) Code() int {
	return o._statusCode
}

func (o *CreateCheckDefault) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/checks][%d] CreateCheck default  %+v", o._statusCode, o.Payload)
}

func (o *CreateCheckDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
