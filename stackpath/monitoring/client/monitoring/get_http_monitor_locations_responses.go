// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// GetHTTPMonitorLocationsReader is a Reader for the GetHTTPMonitorLocations structure.
type GetHTTPMonitorLocationsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetHTTPMonitorLocationsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetHTTPMonitorLocationsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetHTTPMonitorLocationsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewGetHTTPMonitorLocationsInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewGetHTTPMonitorLocationsDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewGetHTTPMonitorLocationsOK creates a GetHTTPMonitorLocationsOK with default headers values
func NewGetHTTPMonitorLocationsOK() *GetHTTPMonitorLocationsOK {
	return &GetHTTPMonitorLocationsOK{}
}

/*GetHTTPMonitorLocationsOK handles this case with default header values.

GetHTTPMonitorLocationsOK get Http monitor locations Ok
*/
type GetHTTPMonitorLocationsOK struct {
	Payload *models.MonitoringGetHTTPMonitorLocationsResponse
}

func (o *GetHTTPMonitorLocationsOK) Error() string {
	return fmt.Sprintf("[GET /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/locations][%d] getHttpMonitorLocationsOk  %+v", 200, o.Payload)
}

func (o *GetHTTPMonitorLocationsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.MonitoringGetHTTPMonitorLocationsResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetHTTPMonitorLocationsUnauthorized creates a GetHTTPMonitorLocationsUnauthorized with default headers values
func NewGetHTTPMonitorLocationsUnauthorized() *GetHTTPMonitorLocationsUnauthorized {
	return &GetHTTPMonitorLocationsUnauthorized{}
}

/*GetHTTPMonitorLocationsUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type GetHTTPMonitorLocationsUnauthorized struct {
	Payload *models.APIStatus
}

func (o *GetHTTPMonitorLocationsUnauthorized) Error() string {
	return fmt.Sprintf("[GET /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/locations][%d] getHttpMonitorLocationsUnauthorized  %+v", 401, o.Payload)
}

func (o *GetHTTPMonitorLocationsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetHTTPMonitorLocationsInternalServerError creates a GetHTTPMonitorLocationsInternalServerError with default headers values
func NewGetHTTPMonitorLocationsInternalServerError() *GetHTTPMonitorLocationsInternalServerError {
	return &GetHTTPMonitorLocationsInternalServerError{}
}

/*GetHTTPMonitorLocationsInternalServerError handles this case with default header values.

Internal server error.
*/
type GetHTTPMonitorLocationsInternalServerError struct {
	Payload *models.APIStatus
}

func (o *GetHTTPMonitorLocationsInternalServerError) Error() string {
	return fmt.Sprintf("[GET /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/locations][%d] getHttpMonitorLocationsInternalServerError  %+v", 500, o.Payload)
}

func (o *GetHTTPMonitorLocationsInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetHTTPMonitorLocationsDefault creates a GetHTTPMonitorLocationsDefault with default headers values
func NewGetHTTPMonitorLocationsDefault(code int) *GetHTTPMonitorLocationsDefault {
	return &GetHTTPMonitorLocationsDefault{
		_statusCode: code,
	}
}

/*GetHTTPMonitorLocationsDefault handles this case with default header values.

Default error structure.
*/
type GetHTTPMonitorLocationsDefault struct {
	_statusCode int

	Payload *models.APIStatus
}

// Code gets the status code for the get Http monitor locations default response
func (o *GetHTTPMonitorLocationsDefault) Code() int {
	return o._statusCode
}

func (o *GetHTTPMonitorLocationsDefault) Error() string {
	return fmt.Sprintf("[GET /monitoring/v1/stacks/{stack_id}/http_monitors/{monitor_id}/locations][%d] GetHttpMonitorLocations default  %+v", o._statusCode, o.Payload)
}

func (o *GetHTTPMonitorLocationsDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
