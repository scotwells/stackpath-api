// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// BulkDeleteHTTPMonitorsReader is a Reader for the BulkDeleteHTTPMonitors structure.
type BulkDeleteHTTPMonitorsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *BulkDeleteHTTPMonitorsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewBulkDeleteHTTPMonitorsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewBulkDeleteHTTPMonitorsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewBulkDeleteHTTPMonitorsInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewBulkDeleteHTTPMonitorsDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewBulkDeleteHTTPMonitorsOK creates a BulkDeleteHTTPMonitorsOK with default headers values
func NewBulkDeleteHTTPMonitorsOK() *BulkDeleteHTTPMonitorsOK {
	return &BulkDeleteHTTPMonitorsOK{}
}

/*BulkDeleteHTTPMonitorsOK handles this case with default header values.

BulkDeleteHTTPMonitorsOK bulk delete Http monitors Ok
*/
type BulkDeleteHTTPMonitorsOK struct {
	Payload models.MonitoringBulkDeleteHTTPMonitorsResponse
}

func (o *BulkDeleteHTTPMonitorsOK) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/bulk_delete][%d] bulkDeleteHttpMonitorsOk  %+v", 200, o.Payload)
}

func (o *BulkDeleteHTTPMonitorsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewBulkDeleteHTTPMonitorsUnauthorized creates a BulkDeleteHTTPMonitorsUnauthorized with default headers values
func NewBulkDeleteHTTPMonitorsUnauthorized() *BulkDeleteHTTPMonitorsUnauthorized {
	return &BulkDeleteHTTPMonitorsUnauthorized{}
}

/*BulkDeleteHTTPMonitorsUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type BulkDeleteHTTPMonitorsUnauthorized struct {
	Payload *models.APIStatus
}

func (o *BulkDeleteHTTPMonitorsUnauthorized) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/bulk_delete][%d] bulkDeleteHttpMonitorsUnauthorized  %+v", 401, o.Payload)
}

func (o *BulkDeleteHTTPMonitorsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewBulkDeleteHTTPMonitorsInternalServerError creates a BulkDeleteHTTPMonitorsInternalServerError with default headers values
func NewBulkDeleteHTTPMonitorsInternalServerError() *BulkDeleteHTTPMonitorsInternalServerError {
	return &BulkDeleteHTTPMonitorsInternalServerError{}
}

/*BulkDeleteHTTPMonitorsInternalServerError handles this case with default header values.

Internal server error.
*/
type BulkDeleteHTTPMonitorsInternalServerError struct {
	Payload *models.APIStatus
}

func (o *BulkDeleteHTTPMonitorsInternalServerError) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/bulk_delete][%d] bulkDeleteHttpMonitorsInternalServerError  %+v", 500, o.Payload)
}

func (o *BulkDeleteHTTPMonitorsInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewBulkDeleteHTTPMonitorsDefault creates a BulkDeleteHTTPMonitorsDefault with default headers values
func NewBulkDeleteHTTPMonitorsDefault(code int) *BulkDeleteHTTPMonitorsDefault {
	return &BulkDeleteHTTPMonitorsDefault{
		_statusCode: code,
	}
}

/*BulkDeleteHTTPMonitorsDefault handles this case with default header values.

Default error structure.
*/
type BulkDeleteHTTPMonitorsDefault struct {
	_statusCode int

	Payload *models.APIStatus
}

// Code gets the status code for the bulk delete Http monitors default response
func (o *BulkDeleteHTTPMonitorsDefault) Code() int {
	return o._statusCode
}

func (o *BulkDeleteHTTPMonitorsDefault) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/http_monitors/bulk_delete][%d] BulkDeleteHttpMonitors default  %+v", o._statusCode, o.Payload)
}

func (o *BulkDeleteHTTPMonitorsDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
