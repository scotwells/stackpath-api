// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewEnableParams creates a new EnableParams object
// with the default values initialized.
func NewEnableParams() *EnableParams {
	var ()
	return &EnableParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewEnableParamsWithTimeout creates a new EnableParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewEnableParamsWithTimeout(timeout time.Duration) *EnableParams {
	var ()
	return &EnableParams{

		timeout: timeout,
	}
}

// NewEnableParamsWithContext creates a new EnableParams object
// with the default values initialized, and the ability to set a context for a request
func NewEnableParamsWithContext(ctx context.Context) *EnableParams {
	var ()
	return &EnableParams{

		Context: ctx,
	}
}

// NewEnableParamsWithHTTPClient creates a new EnableParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewEnableParamsWithHTTPClient(client *http.Client) *EnableParams {
	var ()
	return &EnableParams{
		HTTPClient: client,
	}
}

/*EnableParams contains all the parameters to send to the API endpoint
for the enable operation typically these are written to a http.Request
*/
type EnableParams struct {

	/*StackID*/
	StackID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the enable params
func (o *EnableParams) WithTimeout(timeout time.Duration) *EnableParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the enable params
func (o *EnableParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the enable params
func (o *EnableParams) WithContext(ctx context.Context) *EnableParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the enable params
func (o *EnableParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the enable params
func (o *EnableParams) WithHTTPClient(client *http.Client) *EnableParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the enable params
func (o *EnableParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithStackID adds the stackID to the enable params
func (o *EnableParams) WithStackID(stackID string) *EnableParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the enable params
func (o *EnableParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WriteToRequest writes these params to a swagger request
func (o *EnableParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
