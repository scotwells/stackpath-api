// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "github.com/stackpath/golang_stackpath/stackpath/monitoring/models"
)

// DisableReader is a Reader for the Disable structure.
type DisableReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DisableReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDisableNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewDisableUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewDisableInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		result := NewDisableDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewDisableNoContent creates a DisableNoContent with default headers values
func NewDisableNoContent() *DisableNoContent {
	return &DisableNoContent{}
}

/*DisableNoContent handles this case with default header values.

No content
*/
type DisableNoContent struct {
}

func (o *DisableNoContent) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/disable][%d] disableNoContent ", 204)
}

func (o *DisableNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDisableUnauthorized creates a DisableUnauthorized with default headers values
func NewDisableUnauthorized() *DisableUnauthorized {
	return &DisableUnauthorized{}
}

/*DisableUnauthorized handles this case with default header values.

Returned when an unauthorized request is attempted.
*/
type DisableUnauthorized struct {
	Payload *models.APIStatus
}

func (o *DisableUnauthorized) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/disable][%d] disableUnauthorized  %+v", 401, o.Payload)
}

func (o *DisableUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewDisableInternalServerError creates a DisableInternalServerError with default headers values
func NewDisableInternalServerError() *DisableInternalServerError {
	return &DisableInternalServerError{}
}

/*DisableInternalServerError handles this case with default header values.

Internal server error.
*/
type DisableInternalServerError struct {
	Payload *models.APIStatus
}

func (o *DisableInternalServerError) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/disable][%d] disableInternalServerError  %+v", 500, o.Payload)
}

func (o *DisableInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewDisableDefault creates a DisableDefault with default headers values
func NewDisableDefault(code int) *DisableDefault {
	return &DisableDefault{
		_statusCode: code,
	}
}

/*DisableDefault handles this case with default header values.

Default error structure.
*/
type DisableDefault struct {
	_statusCode int

	Payload *models.APIStatus
}

// Code gets the status code for the disable default response
func (o *DisableDefault) Code() int {
	return o._statusCode
}

func (o *DisableDefault) Error() string {
	return fmt.Sprintf("[POST /monitoring/v1/stacks/{stack_id}/disable][%d] Disable default  %+v", o._statusCode, o.Payload)
}

func (o *DisableDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.APIStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
