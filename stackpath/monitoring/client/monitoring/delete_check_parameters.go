// Code generated by go-swagger; DO NOT EDIT.

package monitoring

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteCheckParams creates a new DeleteCheckParams object
// with the default values initialized.
func NewDeleteCheckParams() *DeleteCheckParams {
	var ()
	return &DeleteCheckParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteCheckParamsWithTimeout creates a new DeleteCheckParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteCheckParamsWithTimeout(timeout time.Duration) *DeleteCheckParams {
	var ()
	return &DeleteCheckParams{

		timeout: timeout,
	}
}

// NewDeleteCheckParamsWithContext creates a new DeleteCheckParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteCheckParamsWithContext(ctx context.Context) *DeleteCheckParams {
	var ()
	return &DeleteCheckParams{

		Context: ctx,
	}
}

// NewDeleteCheckParamsWithHTTPClient creates a new DeleteCheckParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteCheckParamsWithHTTPClient(client *http.Client) *DeleteCheckParams {
	var ()
	return &DeleteCheckParams{
		HTTPClient: client,
	}
}

/*DeleteCheckParams contains all the parameters to send to the API endpoint
for the delete check operation typically these are written to a http.Request
*/
type DeleteCheckParams struct {

	/*CheckID*/
	CheckID string
	/*MonitorID*/
	MonitorID string
	/*StackID*/
	StackID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete check params
func (o *DeleteCheckParams) WithTimeout(timeout time.Duration) *DeleteCheckParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete check params
func (o *DeleteCheckParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete check params
func (o *DeleteCheckParams) WithContext(ctx context.Context) *DeleteCheckParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete check params
func (o *DeleteCheckParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete check params
func (o *DeleteCheckParams) WithHTTPClient(client *http.Client) *DeleteCheckParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete check params
func (o *DeleteCheckParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithCheckID adds the checkID to the delete check params
func (o *DeleteCheckParams) WithCheckID(checkID string) *DeleteCheckParams {
	o.SetCheckID(checkID)
	return o
}

// SetCheckID adds the checkId to the delete check params
func (o *DeleteCheckParams) SetCheckID(checkID string) {
	o.CheckID = checkID
}

// WithMonitorID adds the monitorID to the delete check params
func (o *DeleteCheckParams) WithMonitorID(monitorID string) *DeleteCheckParams {
	o.SetMonitorID(monitorID)
	return o
}

// SetMonitorID adds the monitorId to the delete check params
func (o *DeleteCheckParams) SetMonitorID(monitorID string) {
	o.MonitorID = monitorID
}

// WithStackID adds the stackID to the delete check params
func (o *DeleteCheckParams) WithStackID(stackID string) *DeleteCheckParams {
	o.SetStackID(stackID)
	return o
}

// SetStackID adds the stackId to the delete check params
func (o *DeleteCheckParams) SetStackID(stackID string) {
	o.StackID = stackID
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteCheckParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param check_id
	if err := r.SetPathParam("check_id", o.CheckID); err != nil {
		return err
	}

	// path param monitor_id
	if err := r.SetPathParam("monitor_id", o.MonitorID); err != nil {
		return err
	}

	// path param stack_id
	if err := r.SetPathParam("stack_id", o.StackID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
