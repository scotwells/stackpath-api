MOCKERY_CMD=vendor/github.com/vektra/mockery/cmd/mockery/mockery
SWAGGER_CMD=vendor/github.com/go-swagger/go-swagger/cmd/swagger/swagger
TEMPLATE_ARG=--template-dir ./vendor/bitbucket.stackpath.net/GO/go-swagger-generator-templates/templates/

generate: tools clean
	curl -sS -o identity.swagger.json https://developer.stackpath.com/specs/identity.swagger.json
	curl -sS -o cdn.swagger.json https://developer.stackpath.com/specs/cdn.swagger.json
	curl -sS -o waf.swagger.json https://developer.stackpath.com/specs/waf.swagger.json
	curl -sS -o monitoring.swagger.json https://developer.stackpath.com/specs/monitoring.swagger.json
	curl -sS -o dns.swagger.json https://developer.stackpath.com/specs/dns.swagger.json
	curl -sS -o stack.swagger.json https://developer.stackpath.com/specs/stacks.swagger.json
	curl -sS -o workload.swagger.json https://developer.stackpath.com/specs/workload.swagger.json

	rm -rf ./stackpath

	mkdir -p ./stackpath/identity \
		./stackpath/monitoring \
		./stackpath/dns \
		./stackpath/cdn \
		./stackpath/stack \
		./stackpath/waf \
		./stackpath/compute

	${SWAGGER_CMD} generate client \
		\
		--target=./stackpath/identity \
		--spec=./identity.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/cdn \
		--spec=./cdn.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/waf \
		--spec=./waf.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/monitoring \
		--spec=./monitoring.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/dns \
		--spec=./dns.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/stack \
		--spec=./stack.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

	${SWAGGER_CMD} generate client \
		--target=./stackpath/compute \
		--spec=./workload.swagger.json \
		--additional-initialism=ok \
		${TEMPLATE_ARG}

tools:
	cd vendor/github.com/go-swagger/go-swagger/cmd/swagger && go build
	cd vendor/github.com/vektra/mockery/cmd/mockery && go build

clean:
	rm -rf pkg/client pkg/models

clean-mocks:
	rm -rf pkg/mock_client

validate:
	${SWAGGER_CMD} validate ./swagger.yaml
